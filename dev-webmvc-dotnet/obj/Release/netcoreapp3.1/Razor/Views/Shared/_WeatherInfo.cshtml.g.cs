#pragma checksum "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b375cb060058e11e67f8e57b146a836a5eb9e791"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__WeatherInfo), @"mvc.1.0.view", @"/Views/Shared/_WeatherInfo.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\_ViewImports.cshtml"
using dev_webmvc_dotnet;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\_ViewImports.cshtml"
using dev_webmvc_dotnet.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b375cb060058e11e67f8e57b146a836a5eb9e791", @"/Views/Shared/_WeatherInfo.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cdc7b1e0ec2ff6cb8b31ceb41bba13d41caf9687", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__WeatherInfo : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dev_webmvc_dotnet.Models.Weather>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("smsEmail"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("weather"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"

<div class=""container-fluid"">
    <div class=""row justify-content-center"">
        <div class=""col-12 col-md-4 col-sm-12 col-xs-12"">
            <div class=""card p-4"">
                <div class=""d-flex"">
                    <h6 class=""flex-grow-1"">");
#nullable restore
#line 9 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
                                       Write(Html.DisplayName(Model.Location));

#line default
#line hidden
#nullable disable
            WriteLiteral("</h6>\r\n                    <h6>");
#nullable restore
#line 10 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
                   Write(Html.DisplayName(Model.Time));

#line default
#line hidden
#nullable disable
            WriteLiteral("</h6>\r\n                </div>\r\n                <div class=\"d-flex flex-column temp mt-5 mb-3\">\r\n                    <h1 class=\"mb-0 font-weight-bold\" id=\"heading\"> ");
#nullable restore
#line 13 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
                                                               Write(Html.DisplayName(Model.Temperature));

#line default
#line hidden
#nullable disable
            WriteLiteral("° C </h1> <span class=\"small grey\">");
#nullable restore
#line 13 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
                                                                                                                                      Write(Html.DisplayName(Model.Condition));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</span>
                </div>
                <div class=""d-flex"">
                    <div class=""temp-details flex-grow-1"">
                        </br>
                        <p class=""my-1""> <img src=""https://i.imgur.com/B9kqOzp.png"" height=""17px""> <span> ");
#nullable restore
#line 18 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
                                                                                                     Write(Html.DisplayName(Model.WindSpeed));

#line default
#line hidden
#nullable disable
            WriteLiteral(" km/h </span> </p>\r\n                        <p class=\"my-1\"> <i class=\"fa fa-tint mr-2\" aria-hidden=\"true\"></i> <span> ");
#nullable restore
#line 19 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
                                                                                              Write(Html.DisplayName(Model.Humidity));

#line default
#line hidden
#nullable disable
            WriteLiteral("% </span> </p>\r\n                    </div>\r\n                    <div> <img");
            BeginWriteAttribute("src", " src=\"", 1189, "\"", 1219, 1);
#nullable restore
#line 21 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
WriteAttributeValue("", 1195, Url.Content(Model.Icon), 1195, 24, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" width=""100px""> </div>
                </div>
            </div>
        </div>
    </div>
</div>

</br>


<button id='SendReports' type=""button"" class=""btn btn-light mx-auto d-block btn-lg"">Receive Weather by SMS and Email.</button>

<div class=""modal fade"" id=""getCodeModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""myModalLabel"" aria-hidden=""true"">
    <div class=""modal-dialog modal-lg"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h4 class=""modal-title"" id=""myModalLabel""> Please Enter your Phone Number and Email Address</h4>
                <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close""><span aria-hidden=""true"">&times;</span></button>
            </div>
            <div class=""modal-body"" id=""getCode"" style=""word-wrap:break-word;"">
                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b375cb060058e11e67f8e57b146a836a5eb9e7918092", async() => {
                WriteLiteral(@"
                    <div class=""form-group"">
                        <label for=""emailLabel"">Email address</label>
                        <input type=""email"" class=""form-control"" id=""inputEmail"" aria-describedby=""emailHelp"" placeholder=""Enter email"">
                    </div>
                    <div class=""form-group"">
                        <label for=""smsLabel"">Phone Number</label>
                        <input class=""form-control"" id=""smsInput"" placeholder=""Enter phone number"">
                    </div>
                    <button id=""Send"" class=""btn btn-primary"">Send</button>
");
                WriteLiteral("                ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b375cb060058e11e67f8e57b146a836a5eb9e79110163", async() => {
                WriteLiteral("\r\n    ");
#nullable restore
#line 61 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
Write(Html.HiddenFor(m => m.Condition));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    ");
#nullable restore
#line 62 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
Write(Html.HiddenFor(m => m.Humidity));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    ");
#nullable restore
#line 63 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
Write(Html.HiddenFor(m => m.Icon));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    ");
#nullable restore
#line 64 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
Write(Html.HiddenFor(m => m.Location));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    ");
#nullable restore
#line 65 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
Write(Html.HiddenFor(m => m.Temperature));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    ");
#nullable restore
#line 66 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
Write(Html.HiddenFor(m => m.Time));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    ");
#nullable restore
#line 67 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
Write(Html.HiddenFor(m => m.WindSpeed));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"

<script>

    $('#SendReports').click(function () {


        $(""#getCodeModal"").modal('show');

    });

    $('#Send').click(function () {


        //$(""#getCodeModal"").modal('show');


        var x = $('#weather').serializeArray();
        var indexed_array = {};

        $.map(x, function (n, i) {
            indexed_array[n['name']] = n['value'];
        });

        console.log(indexed_array);

        $.ajax({
            url: '");
#nullable restore
#line 95 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Shared\_WeatherInfo.cshtml"
             Write(Url.Action("Send","SendReports"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\',\r\n            data: indexed_array,\r\n            success: function (data) {\r\n            },\r\n            error: function () {\r\n            }\r\n        });\r\n    });\r\n\r\n\r\n\r\n\r\n\r\n</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dev_webmvc_dotnet.Models.Weather> Html { get; private set; }
    }
}
#pragma warning restore 1591
