#pragma checksum "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Weather\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f42d8a29ebcca8dd622bfccd340403fec35b52ba"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Weather_Index), @"mvc.1.0.view", @"/Views/Weather/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\_ViewImports.cshtml"
using dev_webmvc_dotnet;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\_ViewImports.cshtml"
using dev_webmvc_dotnet.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f42d8a29ebcca8dd622bfccd340403fec35b52ba", @"/Views/Weather/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cdc7b1e0ec2ff6cb8b31ceb41bba13d41caf9687", @"/Views/_ViewImports.cshtml")]
    public class Views_Weather_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dev_webmvc_dotnet.Models.Weather>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f42d8a29ebcca8dd622bfccd340403fec35b52ba3242", async() => {
                WriteLiteral(@"


    <link rel=""stylesheet"" href=""https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"">
    <link rel=""stylesheet"" href=""https://fonts.googleapis.com/css2?family=Marcellus&display=swap"">




    <style>

        html,
        body {
            height: 100%;
            font-family: 'Marcellus', serif
        }

        * {
            padding: 0px;
            margin: 0px
        }

        body {
            background-color: #a5a5b1;
            display: grid;
            place-items: center
        }

        .card {
            background-color: #ffffff;
            border-radius: 50px;
            color: #6f707d;
            font-family: 'Marcellus', serif
        }

        #heading {
            font-size: 55px;
            color: #2b304d
        }

        .temp {
            place-items: center
        }

        .temp-details > p > span,
        .grey {
            color: #a3acc1;
            font-size: 12px
        }");
                WriteLiteral("\n\r\n        .fa {\r\n            color: #a5a5b1\r\n        }\r\n\r\n        *:focus {\r\n            outline: none\r\n        }\r\n    </style>\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n\r\n\r\n\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f42d8a29ebcca8dd622bfccd340403fec35b52ba5441", async() => {
                WriteLiteral("\r\n\r\n");
                WriteLiteral("\r\n\r\n    <button type=\"button\" class=\"btn btn-light mx-auto d-block btn-lg\" onclick=\"getLocation()\">Show Me the Weather!</button>\r\n\r\n    </br>\r\n\r\n    <div id=\"display\"></div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"

<script>


        function getLocation() {
          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(getWeather);
          } else {
              alert(""Geolocation is not supported by this browser."");
          }
        }

    function getWeather(position) {
        $.ajax({
            url: '");
#nullable restore
#line 105 "C:\Genesys\dev-webmvc-dotnet\dev-webmvc-dotnet\Views\Weather\Index.cshtml"
             Write(Url.Action("GetWeather","Weather"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"',
            data: { 'latitude': position.coords.latitude, 'longitude': position.coords.longitude},
            success: function (data) {
                $('#display').html(data);
            },
            error: function () {
                $(""#display"").html(""ERROR"");
            }
        });
    }

    
</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dev_webmvc_dotnet.Models.Weather> Html { get; private set; }
    }
}
#pragma warning restore 1591
