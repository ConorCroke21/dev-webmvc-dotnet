﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dev_webmvc_dotnet.Models
{
    public class Weather
    {
        public string Location { get; set; }
        public string Time { get; set; }
        public string Temperature { get; set; }
        public string Icon { get; set; }
        public string Condition { get; set; }
        public string WindSpeed { get; set; }
        public string Humidity { get; set; }
    }
}
