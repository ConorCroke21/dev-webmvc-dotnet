﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dev_webmvc_dotnet.Models
{
    public class SendReports
    {
        public Request Request { get; set; }

    }

    public partial class Request
    {
        public Properties SMS { get; set; }
        public Properties Email { get; set; }
    }

    public partial class Properties
    {
        public bool enabled { get; set; }
        public string to { get; set; }
        public string from { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
    }
}
