﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dev_webmvc_dotnet.Models
{
    public class StepFuncRequest
    {
        [JsonProperty("input")]
        public string input { get; set; }

        [JsonProperty("stateMachineArn")]
        public string stateMachineArn { get; set; }
    }
}
