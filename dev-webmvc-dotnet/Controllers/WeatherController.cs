﻿using dev_webmvc_dotnet.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dev_webmvc_dotnet.Controllers
{
    public class WeatherController : Controller
    {
        private readonly IConfiguration _config;
        public WeatherController(IConfiguration config)
        {
            _config = config;
        }

        // GET: WeatherController
        public ActionResult Index()
        {
            Weather weather = new Weather();
            weather.Location = "-";
            var dt = DateTime.Now;
            weather.Time = dt.ToString("HH:mm");
            weather.Temperature = "-";
            weather.Icon = "-";
            weather.Condition = "-";
            weather.WindSpeed = "-";
            weather.Humidity = "-";
            return View(weather);
        }

        public async Task<IActionResult> GetWeather(string longitude, string latitude)
        {
            var apiUrl = _config.GetValue<string>("APIUrl");

            var url = $"{apiUrl}weather?longitude={longitude}&latitude={latitude}";
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);

            var weather = JsonConvert.DeserializeObject<Weather>(response.Content);

            int index = weather.Temperature.IndexOf(".");
            if (index > 0)
            {
                weather.Temperature = weather.Temperature.Substring(0, index); 
            }
            
            weather.Icon = $"http://openweathermap.org/img/wn/{weather.Icon}@2x.png";

            var dt = DateTime.Now;
            weather.Time = dt.ToString("HH:mm");


            return PartialView("_WeatherInfo", weather);
        }
    }
}
