﻿using dev_webmvc_dotnet.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dev_webmvc_dotnet.Controllers
{
    public class SendReportsController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Send(Weather weather)
        {
            //Set SendReports Object
            string body = $"The weather is currently {weather.Condition} with temperature of {weather.Temperature} C. Humidity is at {weather.Humidity}% and wind speed is {weather.WindSpeed} km/h.";
            SendReports sendReports = new SendReports();
            sendReports.Request = new Request();
            sendReports.Request.SMS = new Properties();
            sendReports.Request.SMS.enabled = true;
            sendReports.Request.SMS.from = "+353858203415";
            sendReports.Request.SMS.to = "+353858203415";
            sendReports.Request.SMS.body = body;

            sendReports.Request.Email = new Properties();
            sendReports.Request.Email.enabled = true;
            sendReports.Request.Email.to = "croke@live.com";
            sendReports.Request.Email.from = "conorcrokeproject@gmail.com";
            sendReports.Request.Email.subject = "Show Me the Weather!";
            sendReports.Request.Email.body = body;
            
            //Set Stepfunction body request
            var input = JsonConvert.SerializeObject(sendReports);
            var bodyRequest = new StepFuncRequest();
            bodyRequest.input = input;
            bodyRequest.stateMachineArn = "arn:aws:states:eu-west-1:092431187700:stateMachine:SendReports";
            var bodySerialize = JsonConvert.SerializeObject(bodyRequest);

            var client = new RestClient("https://zsqq8tbfm1.execute-api.eu-west-1.amazonaws.com/dev");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", bodySerialize, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            
            return View();
        }
    }
}
