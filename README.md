### dev-webmvc-dotnet ###
This is a Asp.Net Core WebMVC and is written in C# with the webpages written in Razor, HTML and jQuery and is hosted on Azure. This shows an example of a client application making a request 
to dev-webapi-dotnet and displaying the response on a webpage. After clicking button "Show Me the Weather!" it uses your browser to get you Longitude and Latitude and sends request to the API.
There is also the option to send the weather to your phone and email. Please be advised as this is in development the Phone Number and Email address are hardcoded in server side as AWS SES only allows emails to verifyed email addreses and Twilio account does not have credit on it.

You can test the website by going to this URL - https://dev-webmvc-dotnet.azurewebsites.net/weather and clicking button "Show Me the Weather!". Please click allow for the application to allow your coordinates.

![mvc](https://dev-webmvc-dotnet.azurewebsites.net/Images/mvc.PNG){:height="50%" width="50%"}

